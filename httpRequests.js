var request = require('request');
const EventEmitter = require("events");
var obj = new EventEmitter();
module.exports = obj;
function getDataFromWeatherStation() {


    request('http://192.168.1.211/weatherData', function (err, response, body) {
        if (!err && response.statusCode == 200) {
            if (!(body.indexOf('nan') > -1)) {
                console.info(body + " " + new Date());
                obj.emit("weatherDataReady", body);
            }
            else {
                setTimeout(function() {
                 getDataFromWeatherStation();
                }, 1000);
            }
        }
        else {
            console.info(err);
            setTimeout(function() {
                 getDataFromWeatherStation();
                }, 1000);
        }
    });
}

function loopHttpRequest() {
    setTimeout(function () {
        getDataFromWeatherStation()
        loopHttpRequest()
    }, 10000);
}

loopHttpRequest();