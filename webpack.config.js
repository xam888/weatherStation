var webpack = require('webpack');
var path = require('path');

var config = {
  entry: './app.jsx',
  output: {
    path: __dirname,
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : __dirname,
        loader : 'babel-loader',
        exclude: /node_modules/,
         query: {
            cacheDirectory: true,
            presets: ['react', 'es2015']
          }
      },
       { test: /\.css$/, loader: "style-loader!css-loader" }
    ]
  }
};

module.exports = config;