import React from 'react';
import ReactDOM from 'react-dom';
import styles from './style.css';

class App extends React.Component {
    constructor(){
        super();
        this.state = {val: "brak wartości", display: true};
    }

    update(e) {
        this.setState({val: e.target.value});
    }

    toogleDisplay(){
        this.setState({display: !this.state.display})
    }

    render() {
        return (
            <div>
                <input type="textbox" className={'display-'+ this.state.display} onInput={this.update.bind(this)}></input>
                <div>{this.state.val}</div>
                <button onClick={this.toogleDisplay.bind(this)}>toogle</button>
                <SecInput value="Asdas"></SecInput>
            </div>
        );
    }
}

const SecInput = () => <input></input>

SecInput.propTypes = {
    value: React.PropTypes.string.isRequired
}
const app = document.getElementById("app");
ReactDOM.render(<App />, app);

// (function () {
//     var data = [];
//     var ws = new WebSocket('ws://localhost:8082', 'echo-protocol');
//     var tempDiv = document.getElementById("tempDiv");
//     var humDiv = document.getElementById("humDiv");
//     this.counter = null;

//     ws.onopen = function () {
//         ws.send("Message to send");
//         console.info("Message is sent...");
//     };

//     ws.onmessage = function (evt) {
//         console.info("Message is received...");
//         var weatherData = evt.data.split(" ");
//         tempDiv.innerHTML = "Aktualna temperatura wynosi: " + weatherData[0];
//         humDiv.innerHTML = "Aktualna wilgotnosc wynosi: " + weatherData[1];
//         // this.counter++
//         // if (counter % 2 === 0) {
//         //     console.info(evt.data);
//         //      var random = Math.floor((Math.random() * 20)+1);
//         //      var tempValue = random + parseFloat(evt.data)
//         //      document.getElementById("currentTemp").innerHTML = "Aktualna temperatura: " + evt.data
//         // data.push({
//         //     "date": new Date,
//         //     "close": tempValue
//         // })
//         // if(data.length > 10){
//         //     data.shift()
//         // }    
//         // updateChart();
//         // }
//     }.bind();

//     ws.onclose = function () {
//         console.info("Connection is closed...");
//     };

//     //       var m = [80, 80, 80, 80]; // margins
//     // 		var w = 1000 - m[1] - m[3]; // width
//     // 		var h = 400 - m[0] - m[2]; // height
//     // 		console.info(new Date);
//     //        var margin = {top: 20, right: 20, bottom: 30, left: 50},
//     //     width = 960 - margin.left - margin.right,
//     //     height = 500 - margin.top - margin.bottom;

//     // var svg = d3.select("body").append("svg")
//     //     .attr("width", width + margin.left + margin.right)
//     //     .attr("height", height + margin.top + margin.bottom)
//     //   .append("g")
//     //     .attr("transform",
//     //           "translate(" + margin.left + "," + margin.top + ")");




//     // // set the ranges
//     // var x = d3.scaleTime().range([0, width]);
//     // var y = d3.scaleLinear().range([height, 0]);

//     // // define the line
//     // var valueline = d3.line()
//     //     .x(function(d) { return x(d.date); })
//     //     .y(function(d) { return y(d.close); });


//     // // Get the data

//     //   // format the data
//     //   data.forEach(function(d) {
//     //       d.date = d.date;
//     //       d.close = +d.close;
//     //   });

//     //   // Scale the range of the data
//     //   x.domain(d3.extent(data, function(d) { return d.date; }));
//     //   y.domain([0, 50]);

//     //   // Add the valueline path.
//     //   svg.append("path")
//     //       .datum(data)
//     //       .attr("class", "line")
//     //       .attr("d", valueline);

//     //   // Add the X Axis
//     //   svg.append("g")
//     //       .attr("class", "xaxis")
//     //       .attr("transform", "translate(0," + height + ")")
//     //       .call(d3.axisBottom(x));

//     //   // Add the Y Axis
//     //   svg.append("g")
//     //       .call(d3.axisLeft(y));



//     // function updateChart(){
//     //     data.forEach(function(d) {
//     //       d.date = d.date;
//     //       d.close = +d.close
//     //   });
//     //     x.domain(d3.extent(data, function(d) { return d.date; }));

//     //     svg.select(".xaxis").transition().duration(750)
//     //     .call(d3.axisBottom(x).ticks(5));
//     //     svg.select(".line").transition().duration(750)
//     //       .attr("d", valueline(data));
//     // }

// } ());