var WebSocketServer = require('websocket').server;
var http = require('http');
//var sensor = require('./sensor');
var fs = require('fs');
var httpRequests = require('./httpRequests');
var json2csv = require('json2csv');
var fields = ['temperature'];
var execSync = require('child_process').execSync;
var server;

fs.readFile('./index.html', function (err, html) {
    if (err) {
        throw err;
    }
    server = http.createServer(function (request, response) {
        console.log((new Date()) + ' Received request for ' + request.url);
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write();
        response.end();
    });
    server.listen(8082, function () {
        console.log((new Date()) + ' Server is listening on port 8082');
    });
    openWebSocketServer();
});

function saveData(data) {
    var temp = data[0].replace(/\u0000/g, "")
    var dataToSave = {
        "date": new Date(),
        "temperature": temp,
        "humidity": data[1]
    }
    console.info(dataToSave);
    //var csv = json2csv({data: dataToSave, fields: fields});
    var json = JSON.stringify(dataToSave);
    fs.writeFile(process.cwd() + "/tempData.json", json, function (err) {
        if (err) {
            return console.log(err);
        }

        console.log("The file was saved!");
        execSync('json2csv -i tempData.json -f date,temperature,humidity --no-header >> weatherData.csv');
    });
}

function openWebSocketServer(){
wsServer = new WebSocketServer({
    httpServer: server,
    // You should not use autoAcceptConnections for production
    // applications, as it defeats all standard cross-origin protection
    // facilities built into the protocol and the browser.  You should
    // *always* verify the connection's origin and decide whether or not
    // to accept it.
    autoAcceptConnections: false
});

// function originIsAllowed(origin) {
//   // put logic here to detect whether the specified origin is allowed.
//   return true;
// }
wsServer.on('request', function (request) {
    // if (!originIsAllowed(request.origin)) {
    //   // Make sure we only accept requests from an allowed origin
    //   request.reject();
    //   console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
    //   return;
    // }

    var connection = request.accept('echo-protocol', request.origin);
    console.log((new Date()) + ' Connection accepted.');
    connection.on('message', function (message) {

        httpRequests.on("weatherDataReady", function (data) {
            setTimeout(function () {
                var weatherData = data.split(" ");
                saveData(weatherData);
                console.info(weatherData);

            }, 5000);
            //console.info(message)
            connection.sendUTF(data.toString());
        });
        // if (message.type === 'utf8') {
        //     console.log('Received Message: ' + message.utf8Data);
        //     connection.sendUTF(message.utf8Data);
        // }
        // else if (message.type === 'binary') {
        //     console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
        //     connection.sendBytes(message.binaryData);
        // }
    });
    connection.on('close', function (reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
}